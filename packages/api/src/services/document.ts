import {
  promises as fs,
  createReadStream,
  createWriteStream,
  constants,
} from 'fs'
import path from 'path'
import { documentNotFound, invalidDocument } from '../errors'
import { Readable } from 'stream'
import { createHash } from 'crypto'
import uuid from 'uuid/v4'
import sharp from 'sharp'

export type DocumentMetadata = {
  id: string
  filename: string
  mimeType: string
  path: string
}

type DocumentServiceOptions = {
  metadataPath: string
  filesPath: string
  temporaryFilesPath: string
}
class DocumentService {
  private metadataPath: string
  private filesPath: string
  private temporaryFilesPath: string
  constructor(options: DocumentServiceOptions) {
    this.metadataPath = options.metadataPath
    this.filesPath = options.filesPath
    this.temporaryFilesPath = options.temporaryFilesPath
  }

  private getMetadataFilePath(id: string) {
    return path.join(this.metadataPath, `${id}.json`)
  }

  private async getMetadataFile(filename: string) {
    const metadataPath = path.join(this.metadataPath, filename)
    const metadata = await fs.readFile(metadataPath, { encoding: 'utf8' })
    return JSON.parse(metadata)
  }

  private async getMetadata(documentId: string) {
    return this.getMetadataFile(`${documentId}.json`)
  }

  list() {
    const metadataPath = this.metadataPath
    const getMetadataFile = this.getMetadataFile.bind(this)
    return {
      async *[Symbol.asyncIterator](): AsyncGenerator<
        DocumentMetadata,
        void,
        unknown
      > {
        const dir = await fs.opendir(metadataPath)
        for await (const dirent of dir) {
          yield getMetadataFile(dirent.name)
        }
      },
    }
  }

  async getDocument(id: string) {
    if (!(await this.documentExists(id))) {
      throw documentNotFound
    }

    const metadata = await this.getMetadata(id)

    return createReadStream(`${this.filesPath}/${metadata.id}`)
  }

  getStreamHash(fileStream: Readable) {
    return new Promise((resolve, reject) => {
      const contentHash = createHash('md5')
      contentHash.setEncoding('hex')
      fileStream.pipe(contentHash)
      fileStream.on('end', () => {
        resolve(contentHash.read())
      })
      fileStream.on('error', err => {
        reject(err)
      })
    })
  }

  private saveTemporaryDocument(documentStream: Readable) {
    return new Promise<string>(resolve => {
      const filePath = path.join(this.temporaryFilesPath, uuid())
      const writeStream = createWriteStream(filePath)
      documentStream.pipe(writeStream).on('close', () => {
        resolve(filePath)
      })
      return filePath
    })
  }

  private generateDocumentId(path: string) {
    return new Promise<string>(resolve => {
      const file = createReadStream(path)
      const contentHash = createHash('md5')
      contentHash.setEncoding('hex')
      file.on('end', () => {
        contentHash.end()
        const digest = contentHash.read()
        resolve(digest)
      })
      file.pipe(contentHash)
    })
  }

  private async generateMetadata(
    id: string,
    filename: string,
    mimeType: string,
    temporaryFilePath: string
  ) {
    const stat = await fs.stat(temporaryFilePath)
    return {
      id,
      filename,
      mimeType,
      size: stat.size,
    }
  }

  private validateDocument(temporaryFilePath: string, mimeType: string) {
    return new Promise((resolve, reject) => {
      if (!mimeType.startsWith('image/')) {
        resolve(true) // we are not validating other documents yet
        return
      }
      const temporaryImage = createReadStream(temporaryFilePath)
      const image = sharp()
      image.on('error', reject)
      image
        .metadata()
        .then(() => {
          resolve(true)
        })
        .catch(() => {
          resolve(false)
        })
      temporaryImage.pipe(image)
    })
  }

  async saveDocument(
    filename: string,
    mimeType: string,
    documentStream: Readable
  ) {
    const temporaryFilePath = await this.saveTemporaryDocument(documentStream)
    const isValid = await this.validateDocument(temporaryFilePath, mimeType)
    if (!isValid) {
      throw invalidDocument
    }
    const id = await this.generateDocumentId(temporaryFilePath)

    const metadata = await this.generateMetadata(
      id,
      filename,
      mimeType,
      temporaryFilePath
    )

    const permanentFilePath = path.join(this.filesPath, id)
    await fs.rename(temporaryFilePath, permanentFilePath)

    const metadataPath = path.join(this.metadataPath, `${id}.json`)
    await fs.writeFile(metadataPath, JSON.stringify(metadata, null, 2))

    return id
  }

  private async documentExists(id: string) {
    const metadataFilePath = this.getMetadataFilePath(id)

    try {
      await fs.access(metadataFilePath, constants.R_OK)
      const { id }: DocumentMetadata = JSON.parse(
        await fs.readFile(metadataFilePath, 'utf-8')
      )
      const filePath = path.join(this.filesPath, id)
      await fs.access(filePath, constants.R_OK)
    } catch (err) {
      return false
    }

    return true
  }

  async deleteDocument(id: string) {
    if (!(await this.documentExists(id))) {
      throw documentNotFound
    }

    const filePath = path.join(this.filesPath, id)
    const metadataFilePath = this.getMetadataFilePath(id)

    await fs.unlink(filePath)
    await fs.unlink(metadataFilePath)
  }
}

export default DocumentService
