import DocumentService from './document'
import mkdirp from 'mkdirp'
import fs from 'fs'
import path from 'path'
import rimraf from 'rimraf'
import { bufferStream } from './utils'
import { documentNotFound, invalidDocument } from '../errors'

const now = Date.now()
const metadataPath = `/tmp/test-${now}/metadata`
const filesPath = `/tmp/test-${now}/files`
const temporaryFilesPath = `/tmp/test-${now}/temp-files`

describe(DocumentService, () => {
  beforeEach(() => {
    mkdirp.sync(metadataPath)
    mkdirp.sync(filesPath)
    mkdirp.sync(temporaryFilesPath)
  })

  afterEach(() => {
    rimraf.sync(metadataPath)
    rimraf.sync(filesPath)
    rimraf.sync(temporaryFilesPath)
  })

  const documentService = new DocumentService({
    metadataPath,
    filesPath,
    temporaryFilesPath,
  })
  const saveFixture = async (fixtureName: string) => {
    const fixturePath = path.join(__dirname, `../test/fixtures/${fixtureName}`)
    const fileToSave = fs.createReadStream(fixturePath)
    const id = await documentService.saveDocument(
      `saved-${fixtureName}`,
      'document/png',
      fileToSave
    )

    return id
  }

  describe('saveDocument', () => {
    it('saves documents', async () => {
      const fixtureName = 'Smiling_Face_Emoji.png'
      const id = await saveFixture(fixtureName)

      const savedDocument = await bufferStream(
        await documentService.getDocument(id)
      )

      expect(savedDocument).toEqual(
        fs.readFileSync(path.join(__dirname, `../test/fixtures/${fixtureName}`))
      )

      const savedMetadata = require(`${metadataPath}/${id}.json`)
      expect(savedMetadata).toMatchSnapshot()
    })

    it('throws an invalid document error if we try saving an invalid image', async () => {
      const thisFile = fs.createReadStream(__filename)
      await expect(
        documentService.saveDocument('image.png', 'image/png', thisFile)
      ).rejects.toThrowError(invalidDocument)
    })
  })

  describe('list', () => {
    it('lists existing documents', async () => {
      await Promise.all(
        ['Smiling_Face_Emoji.png', 'Neutral_Face_Emoji.png'].map(saveFixture)
      )

      const documentMetadataCollection = []
      for await (const documentMetadata of documentService.list()) {
        documentMetadataCollection.push({ documentMetadata })
      }

      expect(documentMetadataCollection).toMatchSnapshot()
    })
  })

  describe('getDocument', () => {
    it('throws an error when the document is not found', async () => {
      await expect(
        documentService.getDocument('i-sure-do-not-have-this-document.png')
      ).rejects.toThrowError(documentNotFound)
    })
  })

  describe('deleteDocument', () => {
    it('throws an error when the document does not exist', async () => {
      await expect(
        documentService.deleteDocument('i-sure-do-not-have-this-document.png')
      ).rejects.toThrowError(documentNotFound)
    })

    it('deletes an document', async () => {
      const fixtureName = 'Smiling_Face_Emoji.png'
      const id = await saveFixture(fixtureName)
      await documentService.deleteDocument(id)

      await expect(documentService.deleteDocument(id)).rejects.toThrowError(
        documentNotFound
      )
    })
  })
})
