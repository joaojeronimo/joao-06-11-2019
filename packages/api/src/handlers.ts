import e, { Request, Response, Handler } from 'express'
import { DocumentMetadata } from './services/document'
import config from './config'
import Busboy from 'busboy'
import constants from './constants'
import {
  fileExtensionNotAllowed,
  mimeTypeNotAllowed,
  documentNotFound,
} from './errors'
import { Readable } from 'stream'
import safeRegex from 'safe-regex'

const { allowedExtensions, allowedMimeTypes } = constants

const asyncHandlerWrapper = (asyncWrapper: Handler): Handler => (
  req,
  res,
  next
) => Promise.resolve(asyncWrapper(req, res, next)).catch(next)

interface DocumentService {
  list?: () => {
    [Symbol.asyncIterator](): AsyncGenerator<DocumentMetadata, void, unknown>
  }
  saveDocument?: (
    filename: string,
    mimeType: string,
    fileStream: NodeJS.ReadableStream
  ) => Promise<string>
  getDocument?: (id: string) => Promise<Readable>
  deleteDocument?: (id: string) => Promise<void>
}

type HandlersOptions = {
  documentService: DocumentService
}
class Handlers {
  private documentService: DocumentService
  constructor(options: HandlersOptions) {
    this.documentService = options.documentService
  }

  listDocuments = asyncHandlerWrapper(async (req: Request, res: Response) => {
    // Dear reviewer please don't get mad at my (very) improvised generator to JSON
    // streamer. Streaming JSON to responses used to be very natural thing to do back in the old days,
    // but dominictarr/JSONStream has been deprecated and I can't seem to find an alternative.
    // I do know that best way to do this is pagination and blocking the response until I read
    // all the files I need to read. I would never put this on production. But I thought I could
    // have some fun ;)

    const filter: string = req.query.q
    if (!safeRegex(filter)) {
      res.status(400).json({
        error: 'bad filtering expression',
      })
    }

    res.writeHead(200, {
      'Content-Type': 'application/json',
    })
    res.write('[')
    let isFirst = true
    for await (const documentMetadata of this.documentService.list()) {
      if (filter && !documentMetadata.filename.match(filter)) {
        continue
      }

      if (isFirst) {
        isFirst = false
      } else {
        res.write(',')
      }

      res.write(JSON.stringify(documentMetadata))
    }
    res.end(']')
  })

  private saveDocuments(req: Request): Promise<Promise<void>[]> {
    const saveDocument = this.documentService.saveDocument.bind(
      this.documentService
    )
    return new Promise((resolve, reject) => {
      const busboy = new Busboy({
        headers: req.headers,
        ...config.fileUpload,
      })

      const documentsToSave: Promise<void>[] = []

      busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        if (!allowedMimeTypes.includes(mimetype)) {
          reject(mimeTypeNotAllowed)
          return
        }

        const [_, extension] = filename.split('.')
        if (!extension || !allowedExtensions.includes(extension)) {
          reject(fileExtensionNotAllowed)
          return
        }

        documentsToSave.push(saveDocument(filename, mimetype, file))
      })

      // TODO: debug why this is not being called for tests!
      busboy.on('finish', () => {
        resolve(documentsToSave)
      })

      req.pipe(busboy)
    })
  }

  createDocuments = asyncHandlerWrapper(async (req: Request, res: Response) => {
    const documentsToSave = await this.saveDocuments(req)
    const documentIds = await Promise.all(documentsToSave)
    res.status(201).json(documentIds)
  })

  getDocument = asyncHandlerWrapper(async (req: Request, res: Response) => {
    const id = req.params.id
    try {
      const documentStream = await this.documentService.getDocument(id)
      documentStream.pipe(res)
    } catch (err) {
      // TODO: write a middleware that does this automatically:
      if (err === documentNotFound) {
        res.status(404).send({
          error: documentNotFound.message,
        })
        return
      }

      throw err
    }
  })

  deleteDocument = asyncHandlerWrapper(async (req: Request, res: Response) => {
    const id = req.params.id
    try {
      await this.documentService.deleteDocument(id)
      res.sendStatus(204)
    } catch (err) {
      // TODO: write a middleware that does this automatically:
      if (err === documentNotFound) {
        res.status(404).send({
          error: documentNotFound.message,
        })
        return
      }

      throw err
    }
  })
}

export default Handlers
