import dotenv from 'dotenv'
import path from 'path'
import mkdirp from 'mkdirp'

dotenv.config({
  path: path.join(__dirname, '../.env'),
})

const config = {
  cookieSecret: process.env.COOKIE_SECRET,
  port: parseInt(process.env.PORT, 10),
  fileUpload: {
    // these are passed to the instantiation of busboy: https://github.com/mscdex/busboy#busboy-methods
    limits: {
      fields: 0,
      fileSize: parseInt(process.env.MAX_FILE_UPLOAD_SIZE),
    },
  },
  metadataPath: process.env.METADATA_PATH,
  filesPath: process.env.FILES_PATH,
  temporaryFilesPath: process.env.TEMPORARY_FILES_PATH,
  rateLimit: {
    api: {
      windowMs: parseInt(process.env.API_RATE_LIMIT_WINDOW_MS),
      max: parseInt(process.env.API_RATE_LIMIT_MAX_REQUESTS),
    },
    createDocuments: {
      windowMs: parseInt(process.env.CREATE_IMAGES_RATE_LIMIT_WINDOW_MS),
      max: parseInt(process.env.CREATE_IMAGES_RATE_LIMIT_MAX_REQUESTS),
    },
  },
  appAddress: process.env.APP_ADDRESS,
}

mkdirp.sync(config.metadataPath)
mkdirp.sync(config.filesPath)
mkdirp.sync(config.temporaryFilesPath)

export default config
