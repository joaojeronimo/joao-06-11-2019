import { makeAPI } from './'
import request from 'supertest'
import Handlers from './handlers'
import path from 'path'
import { Readable } from 'stream'
import { DocumentMetadata } from './services/document'
import { documentNotFound } from './errors'
const fakeDocuments = [
  {
    id: 'a-string',
    filename: 'a-string',
    mimeType: 'a-string',
    path: 'a-string',
  },
  {
    id: 'b-string',
    filename: 'b-string',
    mimeType: 'b-string',
    path: 'b-string',
  },
]

describe('api', () => {
  describe('GET /documents route', () => {
    it('responds with documents', async () => {
      const api = makeAPI(
        new Handlers({
          documentService: {
            list() {
              return {
                async *[Symbol.asyncIterator](): AsyncGenerator<
                  DocumentMetadata,
                  void,
                  unknown
                > {
                  for await (const document of fakeDocuments) {
                    yield document
                  }
                },
              }
            },
          },
        }),
        false
      )

      const res = await request(api)
        .get('/documents')
        .expect(200)

      expect(res.body).toMatchSnapshot()
    })

    it('filters documents by name when a query is passed', async () => {
      const api = makeAPI(
        new Handlers({
          documentService: {
            list() {
              return {
                async *[Symbol.asyncIterator](): AsyncGenerator<
                  DocumentMetadata,
                  void,
                  unknown
                > {
                  for await (const document of fakeDocuments) {
                    yield document
                  }
                },
              }
            },
          },
        }),
        false
      )

      const res = await request(api)
        .get('/documents?q=a')
        .expect(200)

      expect(res.body).toMatchSnapshot()
    })

    it('responds with an empty array if no documents are found', async () => {
      const api = makeAPI(
        new Handlers({
          documentService: {
            list() {
              return {
                async *[Symbol.asyncIterator](): AsyncGenerator<
                  DocumentMetadata,
                  void,
                  unknown
                > {},
              }
            },
          },
        }),
        false
      )

      const res = await request(api)
        .get('/documents')
        .expect(200)

      expect(res.body).toMatchSnapshot()
    })

    it('responds with an error if an unsafe regex is sent as a query', async () => {
      const api = makeAPI(new Handlers({ documentService: {} }), false)

      const res = await request(api)
        .get('/documents?q=*')
        .expect(400)

      expect(res.body).toMatchSnapshot()
    })
  })

  describe('POST /documents route', () => {
    it.skip('accepts file uploads', async () => {
      // This test works only if we resolve the handler promise after the first file stream finishes.
      // With an actual browser, busboy is behaving normally, so I think supertest is not closing
      // it's payloads, leaving busboy waiting for a form to be finished. I don't have time to debug this
      // right now, will come back to it later.
      const saveDocumentMock = jest.fn(() => Promise.resolve('document id'))

      const api = makeAPI(
        new Handlers({
          documentService: {
            saveDocument: saveDocumentMock,
          },
        }),
        false
      )

      const filename = 'Smiling_Face_Emoji.png'

      const fixturePath = path.join(__dirname, `./test/fixtures/${filename}`)

      await request(api)
        .post('/documents')
        .attach('emoji', fixturePath)
        .expect(201)

      expect(saveDocumentMock).toHaveBeenCalledWith(
        filename,
        'document/png',
        expect.any(Readable)
      )
    })
  })

  describe('GET /document/:id route', () => {
    const expectedId = 'expectedId'
    const fakeDocumentStream = new Readable()
    fakeDocumentStream._read = function() {
      this.push(null)
    }
    const api = makeAPI(
      new Handlers({
        documentService: {
          getDocument: id =>
            id === expectedId
              ? Promise.resolve(fakeDocumentStream)
              : Promise.reject(documentNotFound),
        },
      }),
      false
    )

    it('retrieves document if it exists', async () => {
      await request(api)
        .get(`/documents/${expectedId}`)
        .expect(200)
    })

    it('gets a not found error if it does not exist', async () => {
      await request(api)
        .get(`/documents/do-not-have-this-one`)
        .expect(404, { error: documentNotFound.message })
    })
  })

  describe('DELETE /document/:id route', () => {
    const expectedId = 'expectedId'
    const api = makeAPI(
      new Handlers({
        documentService: {
          deleteDocument: id =>
            id === expectedId
              ? Promise.resolve()
              : Promise.reject(documentNotFound),
        },
      }),
      false
    )

    it('deletes the document if it exists', async () => {
      await request(api)
        .delete(`/documents/${expectedId}`)
        .expect(204)
    })

    it('gets a not found error if it does not exist', async () => {
      await request(api)
        .delete(`/documents/do-not-have-this-one`)
        .expect(404, { error: documentNotFound.message })
    })
  })

  describe('CSRF enabled', () => {
    const saveDocumentMock = jest.fn(() => Promise.resolve('document id'))
    const fakeDocumentStream = new Readable()
    fakeDocumentStream._read = function() {
      this.push(null)
    }
    const api = makeAPI(
      new Handlers({
        documentService: {
          saveDocument: saveDocumentMock,
          getDocument: () => Promise.resolve(fakeDocumentStream),
        },
      })
    )

    const agent = request.agent(api)
    let cookies: string[]

    it('sets CSRF cookies', async () => {
      const res = await agent.get('/documents/id')
      cookies = res.header['set-cookie']
      expect(res.header['set-cookie']).toMatchObject([
        expect.stringMatching(/_csrf=.+?; Path=\/; Secure; SameSite=Strict/),
        expect.stringMatching(/csrf-token=.+?; Path=\/; Secure/),
      ])
    })

    it('does not allow a POST call if we do not send required CSRF parameters', async () => {
      await agent.post('/documents').expect(403)
    })

    // TODO: debug this test
    it.skip('allows a POST call if we send required CSRF parameters', async () => {
      const secret = getCookieValue(
        cookies[0],
        /_csrf=(.+?); Path=\/; Secure; SameSite=Strict/
      )

      const token = getCookieValue(
        cookies[1],
        /csrf-token=(.+?); Path=\/; Secure/
      )

      await agent
        .post('/documents')
        .withCredentials()
        .set('X-CSRF-Token', token)
        .expect(201)
    })
  })
})

const getCookieValue = (cookie: string, expr: RegExp) => {
  const matches = cookie.match(expr)
  return matches[1]
}
