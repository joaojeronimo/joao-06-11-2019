/* istanbul ignore file */

import api from './'
import config from './config'
import https from 'https'
import fs from 'fs'
import path from 'path'

const key = fs.readFileSync(
  path.join(__dirname, '../credentials/localhost.key')
)
const cert = fs.readFileSync(
  path.join(__dirname, '../credentials/localhost.cert')
)

const options = {
  key,
  cert,
  requestCert: false,
  rejectUnauthorized: false,
}

const server = https.createServer(options, api)

server.listen(config.port, function() {
  console.log('Express server listening on', server.address())
})
