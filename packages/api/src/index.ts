import express from 'express'
import Handlers from './handlers'
import DocumentService from './services/document'
import helmet from 'helmet'
import config from './config'
import rateLimit from 'express-rate-limit'
import csurf from 'csurf'
import cookieParser from 'cookie-parser'
import cors from 'cors'

export const makeAPI = (handlers: Handlers, enableCSRF = true) => {
  const api = express()

  api.use(
    cors({
      origin: config.appAddress,
      credentials: true,
    })
  )

  // We enable turning this off only for tests
  if (enableCSRF) {
    api.use(cookieParser(config.cookieSecret))

    const csrfProtection = csurf({
      cookie: {
        signed: true,
        secure: true,
        sameSite: 'strict',
      },
    })

    api.use(csrfProtection)

    api.use((req, res, next) => {
      res.cookie('csrf-token', req.csrfToken(), {
        secure: true,
        sameSite: true,
      })
      next()
    })
  }

  api.use(rateLimit(config.rateLimit.api))
  api.use(helmet())

  // This header is used to slow down responses by the number of seconds in it's value
  api.use((req, res, next) => {
    const slowResponse = req.headers['x-slow-response']
    if (typeof slowResponse === 'string') {
      setTimeout(next, parseInt(slowResponse, 10) * 1000)
      return
    }

    next()
  })

  api.post(
    '/documents',
    rateLimit(config.rateLimit.createDocuments),
    handlers.createDocuments
  )
  api.get('/documents', handlers.listDocuments)
  api.get('/documents/:id', handlers.getDocument)
  api.delete('/documents/:id', handlers.deleteDocument)

  return api
}

const documentService = new DocumentService({
  metadataPath: config.metadataPath,
  filesPath: config.filesPath,
  temporaryFilesPath: config.temporaryFilesPath,
})
const handlers = new Handlers({ documentService })

const api = makeAPI(handlers)

export default api
