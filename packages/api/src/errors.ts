export const fileExtensionNotAllowed = new Error('file extension not allowed')
export const mimeTypeNotAllowed = new Error('mime-type not allowed')
export const documentNotFound = new Error('document not found')
export const invalidDocument = new Error('invalid document')
