import React, { PureComponent } from 'react'
import { Reset } from 'styled-reset'
import GlobalStyle from './components/global-style'
import Search from './components/search'
import Upload from './components/upload'
import DocumentsHeader from './components/documents-header'
import TotalSize from './components/total-size'
import Documents from './components/documents'
import { UploadedDocument } from './components/document'
import styled from 'styled-components'
import GlobalLoading from './components/global-loading'

const AppContainer = styled.div`
  display: grid;
  height: 100%;
  grid-template-columns: auto 480px 480px auto;
  grid-template-rows: 150px 100px auto;
  grid-template-areas:
    '. search upload .'
    '. documents-header total-size .'
    '. documents documents .';

  @media screen and (max-width: 960px) {
    grid-template-columns: auto 480px auto;
    grid-template-rows: 50px 50px 60px 40px auto;
    grid-template-areas:
      '. upload .'
      '. search .'
      '. documents-header .'
      '. total-size .'
      '. documents .';
  }
`

interface APIClient {
  listDocuments: (query?: string) => Promise<UploadedDocument[]>
  createDocuments: (body: FormData) => Promise<void>
  deleteDocument: (id: string) => Promise<void>
}

type AppProps = {
  api: APIClient
}

type AppState = {
  isLoadingDocuments: boolean
  isDeletingDocument: boolean
  isUploadingFiles: boolean
  documents: UploadedDocument[]
  totalSize: number
}

class App extends PureComponent<AppProps, AppState> {
  state: AppState = {
    isLoadingDocuments: false,
    isDeletingDocument: false,
    isUploadingFiles: false,
    documents: [],
    totalSize: 0,
  }

  loadDocuments = async (query?: string) => {
    this.setState({ isLoadingDocuments: true })
    const { api } = this.props
    api
      .listDocuments(query)
      .then(documents => {
        const totalSize = documents.reduce(
          (acc, document) => acc + document.size,
          0
        )
        this.setState({ documents, isLoadingDocuments: false, totalSize })
      })
      .catch(err => {
        alert(`error fetching documents ${err}`)
      })
  }

  setDeletingState = (isDeletingDocument: boolean) => {
    this.setState({ isDeletingDocument })
  }

  setUploadingState = (isUploadingFiles: boolean) => {
    this.setState({ isUploadingFiles })
  }

  handleSearchChange = (query: string) => {
    this.loadDocuments(query)
  }

  componentDidMount() {
    this.loadDocuments()
  }

  render() {
    const {
      documents,
      isLoadingDocuments,
      isDeletingDocument,
      isUploadingFiles,
      totalSize,
    } = this.state
    const isLoading =
      isLoadingDocuments || isDeletingDocument || isUploadingFiles
    return (
      <>
        <Reset />
        <GlobalStyle />
        <GlobalLoading isLoading={isLoading} />
        <AppContainer>
          <Search handleSearchChange={this.handleSearchChange} />
          <Upload
            loadDocuments={this.loadDocuments}
            setUploadingState={this.setUploadingState}
          />
          <DocumentsHeader numberOfDocuments={documents.length} />
          <TotalSize totalSize={totalSize} />
          <Documents
            documents={documents}
            loadDocuments={this.loadDocuments}
            setDeletingState={this.setDeletingState}
          />
        </AppContainer>
      </>
    )
  }
}

export default App
