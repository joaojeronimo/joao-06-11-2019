import React from 'react'
import { shallow } from 'enzyme'
import Upload, { UploadFilesButton, UploadInput } from './upload'

describe(Upload, () => {
  const createDocuments = jest.fn((body: FormData) => Promise.resolve())
  const loadDocuments = jest.fn()
  const setUploadingState = jest.fn((isUploadingFiles: boolean) => {})
  const wrapper = shallow(
    <Upload
      createDocuments={createDocuments}
      loadDocuments={loadDocuments}
      setUploadingState={setUploadingState}
    />
  )

  it('renders', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('shows the upload input element when the upload files button is clicked', () => {
    expect(wrapper.find(UploadInput).exists()).toBe(false)
    wrapper.find(UploadFilesButton).simulate('click')
    expect(wrapper.find(UploadInput).exists()).toBe(true)
  })

  it('starts a file upload when files are selected', () => {
    wrapper.find(UploadInput).simulate('change', { target: { files: [] } })
    expect(setUploadingState).toHaveBeenCalledWith(true)
    expect(createDocuments).toHaveBeenCalled()
  })
})
