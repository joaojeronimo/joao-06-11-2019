import React, { useState } from 'react'
import styled, { css, keyframes } from 'styled-components'
import api from '../api-client'

const disappearing = keyframes`
  from {
    opacity:1;
  }
  to {
    opacity:0;
  }
`

const DocumentWrapper = styled.div<{ beingDeleted: boolean }>`
  height: 150px;
  border: 2px solid #ccc;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  display: grid;
  grid-template-rows: 50% 50%;
  grid-template-columns: auto 45% 45% auto;
  grid-template-areas:
    '. title title .'
    '. size delete .';
  ${({ beingDeleted }) =>
    beingDeleted
      ? css`
          animation: ${disappearing} 1s linear;
          animation-fill-mode: forwards;
        `
      : ''}
`

const DocumentTitle = styled.h3`
  grid-area: title;
  font-size: 16pt;
  align-self: end;
`

const Size = styled.div`
  grid-area: size;
  align-self: center;
`

const Delete = styled.div`
  grid-area: delete;
  align-self: center;
  justify-self: end;
`

export type UploadedDocument = {
  id: string
  filename: string
  size: number
}

export type DocumentProps = UploadedDocument & {
  loadDocuments: () => void
  setDeletingState: (isDeletingDocument: boolean) => void
}

const Document = ({
  id,
  filename,
  size,
  loadDocuments,
  setDeletingState,
}: DocumentProps) => {
  const [deletingState, startFadeout] = useState(false)

  const handleTelete = () => {
    setDeletingState(true)
    startFadeout(true)
    api
      .deleteDocument(id)
      .then(() => {
        setDeletingState(false)
        loadDocuments()
      })
      .catch(err => {
        setDeletingState(false)
        alert('failed to delete')
      })
  }

  const inMB = (size / (1024 * 1024)).toFixed(2)

  return (
    <DocumentWrapper beingDeleted={deletingState}>
      <DocumentTitle>{filename}</DocumentTitle>
      <Size>{inMB} MB</Size>
      <Delete>
        <button onClick={handleTelete}>delete</button>
      </Delete>
    </DocumentWrapper>
  )
}

export default Document
