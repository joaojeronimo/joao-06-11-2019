import React, { ChangeEvent, useState } from 'react'
import styled from 'styled-components'
import api from '../api-client'

const UploadContainer = styled.div`
  grid-area: upload;
  align-self: center;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`

export const UploadInput = styled.input`
  width: 100%;
`

export const UploadFilesButton = styled.button``

type UploadProps = {
  createDocuments: (body: FormData) => Promise<void>
  loadDocuments: () => void
  setUploadingState: (isUploadingFiles: boolean) => void
}

const Upload = ({
  createDocuments,
  loadDocuments,
  setUploadingState,
}: UploadProps) => {
  const [selectingFiles, selectFiles] = useState(false)

  const handleChange = async (e: ChangeEvent<HTMLInputElement>) => {
    const { files } = e.target
    if (!files) {
      return
    }
    const formData = new FormData()
    for (let i = 0; i < files.length; i++) {
      const file = files[i]
      formData.append(file.name, file)
    }

    try {
      setUploadingState(true)
      await createDocuments(formData)
    } catch (err) {
      alert('failed to upload files')
      setUploadingState(false)
    }
    setUploadingState(false)
    selectFiles(false)
    loadDocuments()
  }

  return (
    <UploadContainer>
      {selectingFiles ? (
        <form>
          <UploadInput
            onChange={handleChange}
            name="upload files"
            type="file"
            accept="image/png, image/jpeg"
            aria-label="Select files to upload"
            multiple
          />
        </form>
      ) : (
        <UploadFilesButton onClick={() => selectFiles(true)}>
          Upload files
        </UploadFilesButton>
      )}
    </UploadContainer>
  )
}

Upload.defaultProps = {
  createDocuments: api.createDocuments.bind(api),
}

export default Upload
