import React from 'react'
import styled from 'styled-components'

const TotalSizeContainer = styled.div`
  grid-area: total-size;
  align-self: center;
  font-size: 16pt;
  display: flex;

  @media screen and (min-width: 960px) {
    height: 32px;
    justify-content: flex-end;
    align-items: flex-end;
  }

  @media screen and (max-width: 960px) {
    flex-direction: column;
    justify-content: flex-start;
  }
`

type TotalSizeProps = {
  totalSize: number
}

const TotalSize = ({ totalSize }: TotalSizeProps) => {
  const inMB = (totalSize / (1024 * 1024)).toFixed(2)
  return <TotalSizeContainer>Total size: {inMB} MB</TotalSizeContainer>
}

export default TotalSize
