import React from 'react'
import styled from 'styled-components'
import Document, { UploadedDocument } from './document'

const DocumentsContainer = styled.div`
  grid-area: documents;
  display: grid;
  grid-template-columns: repeat(3, 300px);
  grid-row-gap: 20px;
  justify-content: space-between;
  align-content: space-between;
  @media screen and (max-width: 960px) {
    grid-template-columns: 100%;
    justify-content: center;
  }
`

type DocumentsProps = {
  documents?: UploadedDocument[]
  loadDocuments: () => void
  setDeletingState: (isDeletingDocument: boolean) => void
}

const Documents = ({
  documents,
  loadDocuments,
  setDeletingState,
}: DocumentsProps) => {
  if (!documents) {
    return <div>Spinner goes here!</div>
  }

  return (
    <DocumentsContainer>
      {documents.map(document => (
        <Document
          key={document.id}
          id={document.id}
          filename={document.filename}
          size={document.size}
          loadDocuments={loadDocuments}
          setDeletingState={setDeletingState}
        />
      ))}
    </DocumentsContainer>
  )
}

export default Documents
