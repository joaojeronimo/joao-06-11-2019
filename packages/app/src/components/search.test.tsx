import React from 'react'
import { shallow } from 'enzyme'
import Search, { SearchInput } from './search'

describe(Search, () => {
  const handleSearchChange = jest.fn((query: string) => {})
  const debounce = jest.fn(fn => fn)
  const wrapper = shallow(
    <Search handleSearchChange={handleSearchChange} debounce={debounce} />
  )

  it('renders', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('calls handleSearchChange with safe regex expressions', () => {
    const query = 'aaaaa'
    wrapper.find(SearchInput).simulate('change', { target: { value: query } })
    expect(handleSearchChange).toHaveBeenCalledWith(query)
    expect(debounce).toHaveBeenCalled()
  })

  it('calls does not call handleSearchChange with unsafe regex expressions', () => {
    const query = '*'
    wrapper.find(SearchInput).simulate('change', { target: { value: query } })
    expect(handleSearchChange).not.toHaveBeenCalledWith(query)
  })
})
