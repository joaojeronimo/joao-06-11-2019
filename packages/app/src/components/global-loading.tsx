import React from 'react'
import styled, { keyframes } from 'styled-components'

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`

const Spinner = styled.div`
  border: 8px solid white;
  border-top: 8px solid black;
  border-radius: 50%;
  width: 240px;
  height: 240px;
  animation: ${spin} 2s linear infinite;
`

const Overlay = styled.div<{ isLoading: boolean }>`
  display: ${({ isLoading }) => (isLoading ? 'flex' : 'none')};
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.8);
  justify-content: center;
  align-items: center;
`

type GlobalOverlayProps = {
  isLoading: boolean
}

const GlobalOverlay = ({ isLoading }: GlobalOverlayProps) => (
  <Overlay isLoading={isLoading}>
    <Spinner />
  </Overlay>
)

export default GlobalOverlay
