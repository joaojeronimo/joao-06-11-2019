import React from 'react'
import styled from 'styled-components'

const DocumentsHeaderContainer = styled.h1`
  grid-area: documents-header;
  align-self: center;
  font-size: 24pt;
`

type DocumentsHeaderProps = {
  numberOfDocuments: number
}

const DocumentsHeader = ({ numberOfDocuments }: DocumentsHeaderProps) => (
  <DocumentsHeaderContainer>
    {numberOfDocuments} documents
  </DocumentsHeaderContainer>
)

export default DocumentsHeader
