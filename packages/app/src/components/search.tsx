import React, { ChangeEvent, useState } from 'react'
import styled from 'styled-components'
import safeRegex from 'safe-regex'
import { debounce } from 'lodash'

const SearchContainer = styled.div`
  grid-area: search;
  align-self: center;
`

export const SearchInput = styled.input<{ isSafe: boolean }>`
  width: 100%;
  ${({ isSafe }) => (!isSafe ? 'border: 1px solid red;' : '')}
`

type SearchProps = {
  handleSearchChange: (query: string) => void
  debounce: (...args: any) => any
}

const Search = ({ handleSearchChange, debounce }: SearchProps) => {
  const [isSafe, setIsSafe] = useState(true)

  const debouncedHandleSearchChange = debounce(
    (query: string) => handleSearchChange(query),
    200
  )

  const onChange = function(e: ChangeEvent<HTMLInputElement>) {
    const query = e.target.value
    const isSafe = safeRegex(query)
    setIsSafe(safeRegex(query))
    if (isSafe) {
      debouncedHandleSearchChange(query)
    }
  }

  const onSubmit = (e: React.FormEvent) => e.preventDefault()

  return (
    <SearchContainer>
      <form onSubmit={onSubmit}>
        <SearchInput
          isSafe={isSafe}
          onChange={onChange}
          placeholder="search for documents"
        />
      </form>
    </SearchContainer>
  )
}

// we need to inject a fake one for the tests
Search.defaultProps = {
  debounce,
}

export default Search
