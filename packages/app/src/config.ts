const isProd = process.env.NODE_ENV === 'production'

const config = isProd
  ? {
      baseAPIPath: 'https://tld.com',
    }
  : {
      baseAPIPath: 'https://localhost:8000',
    }

export default config
