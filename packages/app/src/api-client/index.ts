import config from '../config'
import fetch from './fetch'
import cookie from 'cookie'
import { UploadedDocument } from '../components/document'

const getToken = () => cookie.parse(document.cookie)['csrf-token']

type APIClientOptions = {
  baseAPIPath: string
}

export class APIClient {
  private baseAPIPath: string
  constructor(options: APIClientOptions) {
    this.baseAPIPath = options.baseAPIPath
  }

  async listDocuments(filter?: string) {
    const q = filter ? encodeURIComponent(filter) : ''
    const res = await fetch<UploadedDocument[]>(
      `${this.baseAPIPath}/documents?q=${q}`
    )
    return res.json()
  }

  async createDocuments(body: FormData) {
    const token = getToken()
    await fetch(`${this.baseAPIPath}/documents`, {
      method: 'POST',
      body,
      credentials: 'include',
      headers: {
        'X-CSRF-Token': token,
      },
    })
  }

  async deleteDocument(id: string) {
    const token = getToken()
    await fetch(`${this.baseAPIPath}/documents/${id}`, {
      method: 'DELETE',
      credentials: 'include',
      headers: {
        'X-CSRF-Token': token,
      },
    })
  }
}

export default new APIClient({ baseAPIPath: config.baseAPIPath }) // TODO: more consistent naming here
