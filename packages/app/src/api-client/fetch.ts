import { merge } from 'lodash'

export default async function<JSON = any>(
  input: RequestInfo,
  init?: RequestInit
): Promise<Response> {
  const slowResponse = localStorage.getItem('slowResponse')
  return fetch(
    input,
    merge({}, init, {
      credentials: 'include',
      headers: {
        'X-Slow-Response': slowResponse,
      },
    })
  )
}
