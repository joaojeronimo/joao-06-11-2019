import React from 'react'
import App from './app'

describe(App, () => {
  const fakeApi = {
    listDocuments: () => Promise.resolve([{ id: 'id', filename: 'filename', size: 239 }]),
    createDocuments: (body: FormData) => Promise.resolve(),
    deleteDocument: (id: string) => Promise.resolve(),
  }
  it('renders', () => {
    expect(<App api={fakeApi} />).toMatchSnapshot()
  })
})
