# João - 06-11-2019

## Development

    yarn

### Running

    yarn start

### Testing

    yarn test

### Linting

    yarn lint

### Seeing it in action

In development mode, we need to get our browser to trust some self signed certificates. Having started everything in development mode with `yarn start`, head over to:

- (API) https://localhost:8000 and accept the risk
- (App) https://localhost:3000 and accept the risk

You can now click through the UI.

If you are seeing some ugly loading state transitions flickering too fast, you can slow down all API responses by opening the javascript console and setting the `slowResponse` localStorage key to the number of seconds you would like to slow down your responses, e.g. `localStorage.setItem('slowResponse', 1)`.

## Security

### Addressed

- Assorted vulnerabilities prevented by [helmet](https://github.com/helmetjs/helmet):
  - hidePoweredBy: removes the X-Powered-By header;
  - HSTS: sets Strict-Transport-Security header that enforces HTTPS connections;
  - ieNoOpen: sets X-Download-Options for IE8+;
  - noSniff: sets X-Content-Type-Options to prevent browsers from MIME-sniffing a response away from the declared content-type;
  - frameguard: sets the X-Frame-Options header to provide clickjacking protection;
  - xssFilter: sets X-XSS-Protection to enable the XSS filter in most recent web browsers;
- CSP: there is a policy in the root app document. However it's a little relaxed because for development, a few scripts are injected in the page;
- HTTPS: there are self signed certificates in the repository for development environment;
- Dependency vulnerabilities: `yarn audit` runs on a pre-commit hook;
- Filter accepted files: only `jpg` and `png` files are accepted (checking extensions and mime types);
- File upload size: the maximum file upload size is 10MB (TODO: work on overall upload size as multiple files can be added);
- Rate limiting: the whole API is rate limited to 100 requests every 15 minutes, the create images endpoint is rate limited to 1 request every 10 seconds;
- CSRF protection:
  - the CSRF secret is stored in a secure, httpOnly, samesite cookie
  - the CSRF token is sent to the application as a secure, samesite cookie
  - the server ignores CSRF tokens on request cookies, instead they have to be haded back either in the request body or headers.
- CORS
- Search input sanitization: we use a simple [RegExp test](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test) for matching search inputs against filenames, so it's impossible to make sure these expressions are safe to execute. [Safe regex](https://github.com/substack/safe-regex) in both the front and backend
- Images saved are really images. Besides filtering for the specific mime-types on the frontend and backend, before saving an image in it's final location, it gets saved in a temporary location for processing (which if this was a serious thing, would be done in a queue). Processing here means passing the image through [sharp](https://github.com/lovell/sharp) to figure out if the contents of the file are valid or not, regardless of the file extension and mime type.

### Not addressed

- [Insufficient logging and monitoring](https://www.owasp.org/index.php/Top_10-2017_A10-Insufficient_Logging%26Monitoring) - If this was going to production I would add access logging at a frontend proxy level and info logs around filesystem access and content verification;
- Listing documents should be paginated;
- Penetration testing (got no budget);
- Other abuse vectors (of course we are limited here by having no auth):
  - robots: something like recaptcha;
  - fingerprinting browsers;

## Improvements

- Error handling. I ended up not having time to do proper error handling, so I left some alerts in the code, sorry =/
- Loading state. I am the worst at design (I can follow specs just fine though), so I couldn't figure out a good way to implement loading states... so I just did one global spinner for all the application's loading states;
- Productionize this application
  - Run compiled typescript code with NODE_ENV=production
  - Use a JSON logger like winston
- Testing end to end: docker-compose with the two containers and a headless browser;
- We run the images through sharp to make sure they are images, we could use that to get more metadata and save it as well;
- Better search with something like [fusejs](https://fusejs.io/);

## Libraries

### Backend

- Express: it is very simple, not willing to take any risks here;
- helmet, csurf (used with cookie-parser), cors, express-rate-limit: express security middlewares;
- busboy: a streaming multi-part form parser;
- safe-regex: used to validate expressions for document search;
- sharp: used to make sure the file contents of an image is in fact an image;
- uuid: used to generate temporary document IDs;

### Frontend

- styled-components: I use it at work, thought it would be the fastest to get going here;
- cookie: to retrieve a CSRF token;
- lodash: I believe I just use debounce from it (hopefully webpack does some tree shaking magic and gets rid of all the rest);
- safe-regex: no need to send search queries if they are invalid;
- styled-reset: Reset CSS without having to copy over a lot of code;

## API

### POST /documents

- Creates an document
- Accepts a multipart/form-data request body
- Responds with 201 if successful, 400 if not

### GET /documents

- lists documents
- responds with 200 and an array of documents
- if a q parameter is passed with search terms, it filters filenames by search terms

### GET /documents/{id}

- retrieves an image
- requires the id path parameter
- responds with 200 and the image, 404 if not found

### DELETE /documents/{id}

- deletes an image
- requires the id path parameter
- responds with 204 if successful, 404 if not found

## Other notes

- I will submit a development mode setup, where things are run with self signed certificates; In production I wouldn't have node doing TLS termination (or rate limiting and other security features).
- There are no synchronous or blocking operations in the server :) the downside is perhaps code complexity is a bit higher and testing is a bit more complicated as well.
- There is a problem with supertest not closing a form, so a "uploads file" test is skipped.

## Assumptions

- The spec says we should allow only `jpg` or `png` files. The other extensions (`jpeg`, `jfif`, `pjpeg` and `pjp`) also have mime-type `image/jpeg` but because the spec specifically says only `jpg`, those other extensions will not be alloed. But it's all configurable [here](/packages/api/src/constants.ts).

# TODO

(wish I had time)

- make jest understand typescript when running inside a package folder instead of just at top level
- respond with proper errors when responses fail with 400.
- add a linter rule to sort my imports
- cover forbiden mime types and extensions in server tests
