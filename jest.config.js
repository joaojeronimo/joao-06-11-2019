module.exports = {
  collectCoverageFrom: ['**/packages/*/**/*.ts', '**/packages/*/**/*.tsx'],
  modulePathIgnorePatterns: ['packages/.*/lib'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  setupFiles: ['./packages/app/setup-tests.js'],
}
